package lab04.zad1;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class MathApplication {
    private final Calculator calculator;
    public MathApplication(final Calculator calculator) {
        this.calculator = calculator;
    }
    @PostConstruct
    public void init() {
        final int a = 2;
        final int b = 3;
        System.out.println("a+b="+this.calculator.add(a, b));
        System.out.println("a-b="+this.calculator.subtrack(a, b));
        System.out.println("a*b="+this.calculator.multiply(a, b));
        System.out.println("a/b="+this.calculator.divide(a, b));
    }
}
