package lab04.zad2;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Qualifier;
import javax.annotation.PostConstruct;
import java.util.Arrays;

@Component
public class SortManager {
    @Qualifier
    private final SortAlgorithm sortAlgorithm;
    public SortManager(@Qualifier("bubble-descending") final SortAlgorithm sortAlgorithm) {
        this.sortAlgorithm = sortAlgorithm;
    }
    @PostConstruct
    public void init() {
        final int[] array = {5, 9, 1, 3, 2};
        sortAlgorithm.sort(array);
        System.out.println(Arrays.toString(array));
    }
}
