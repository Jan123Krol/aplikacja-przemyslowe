package lab04.zad2;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Qualifier;


@Component
@Qualifier("bubble-ascending")
public class BubbleAscendingAlgorithm implements SortAlgorithm {
    @Override
    public void sort(final int[] tab) {
        for (int i = 0; i < tab.length; ++i) {
            for (int j = 0; j < tab.length-1; ++j) {
                if (tab[j] > tab[j+1]) {
                    final int tmp = tab[j];
                    tab[j] = tab[j+1];
                    tab[j+1] = tmp;
                }
            }
        }
    }
}
