package lab04.zad2;

public interface SortAlgorithm {
    void sort(final int[] tab);
} 